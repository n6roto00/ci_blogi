<?php

    class query extends CI_Model {
        
        
        public function getPosts() {
            
            $query = $this->db->get('kirjoitus');
                
            if ($query->num_rows() > 0) {
                return $query->result();
            }
        }
        
                
        public function getUsers() {

            $query = $this->db->get('kayttaja');
            if ($query->num_rows() > 0) {
                return $query->result();
            }
        }
        
        
        public function getUser($post_id) {
            // produces a where-string
            $query = $this->db->get_where('tbl_users');
            if ($query->num_rows() > 0) {
                return $query->row();
            }
        }
        
        
        
        
        public function getSinglePost($post_id) {
            $query = $this->db->get_where('kirjoitus', array('id' => $post_id));
            if ($query->num_rows() > 0) {
                return $query->row();
            }
        }
        
        
        
        
        public function addPost($data) {
            $this->db->insert('kirjoitus', $data);
        }
        
        
        public function updatePost($data, $post_id) {
            $this->db->update('kirjoitus', $data, array('id' => $post_id));
        }


        
        
        public function getComments($post_id) {

            $query3 = $this->db->query("select * from kommentti");                
                        
            if ($query3->num_rows() > 0) {
                return $query3->result();
            }
        }
        
        
        
        
        public function addComment($data) {
            $this->db->insert('kommentti', $data);
        }
        
        
        
        
        public function deletePost($data,$post_id) {
            $this->db->delete('kommentti', array('id' => $post_id));
            $this->db->delete('kirjoitus', array('id' => $post_id));
        }
        
        
        
        
        public function registerUser($data) {
            return $this->db->insert('kayttaja', $data);
        }
        
        
        
        
        public function login($username, $password) {
            $query = $this->db->where(['tunnus'=>$username, 'salasana'=>$password])
                                ->get('kayttaja');
            
            if ($query->num_rows() > 0) {
                return $query->row();
            }
        }
        
        
    }

