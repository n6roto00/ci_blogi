<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
    
    public function index()
	{
            $this->load->model('query');   
            $queriedPosts = $this->query->getPosts();
            $this->load->view('login', ['postsArray'=>$queriedPosts]);
	}
        
        
        
        
        
         public function registerPage() {          
            $this->load->view('register');
        }
        
        
        
        
        
        
        
        public function registerUser() {
            $this->form_validation->set_rules('tunnus', 'Username', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('salasana', 'Password', 'required');
            
            if ($this->form_validation->run()) {
                
                $data = $this->input->post();
                unset($data['submit']);
                $this->load->model('query');
                $this->query->registerUser($data);
                return redirect('Blogi');
            }
            else {
                echo validation_errors();
                echo anchor('Blogi', 'Return', ['class'=>'btn btn-sm btn-warning btn-block']);
            }
        }
        
        
        
        
        
        
        
        public function login() {
            $this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            if ($this->form_validation->run()) {
                $this->load->model('query');
                
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                
                $user = $this->query->login($username, $password);

                if ($user) {
                    $session_data = array(
                        'user_id' => $user->id,
                        'username' => $user->tunnus,
                        'email' => $user->email,
                        'password' => $user->salasana,
                    );
                    $this->session->set_userdata($session_data);
                    return redirect('Blogi');
                    
                } else {
                    $this->load->view('loginFailed');
                }
                
                
            }
            else  {
                $this->load->view('loginFailed');
            }
        }
        
        
        public function logout() {
            $this->session->sess_destroy();
            return redirect('Blogi');
        }
    
    
}


