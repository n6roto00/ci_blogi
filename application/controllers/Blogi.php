<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogi extends CI_Controller {

	public function index()
	{
            $this->load->model('query');   
            $queriedPosts = $this->query->getPosts();
            $this->load->view('blog_page', ['postsArray'=>$queriedPosts]);
	}
        
        public function getComments() {
            $this->load->model('query');
            
        }
        
        public function create() {
            $this->load->view('create');
        }
        
        public function save() {

            $this->form_validation->set_rules('otsikko', 'Otsikko', 'required');
            $this->form_validation->set_rules('teksti', 'Teksti', 'required');
            $this->form_validation->set_rules('kayttaja_id', 'Kayttaja_id', 'required');
            
            
            
            if ($this->form_validation->run())
                {
                    $data = $this->input->post();
                    unset($data['formSubmitButton']);
                    unset($data['approved']);
                    
                    // vaihtehtoinen tapa välittää date, jos sitä ei ole databaseen määritelty
                    //$today = date('d-m-Y');
                    //$data['date'] = $today;
                    
                    $this->load->model('query');

                    
                    $this->query->addPost($data);

                    return redirect('Blogi');
                }
                else
                {
                    echo $this->input->post('otsikko');
                    echo $this->input->post('teksti');
                    echo $this->input->post('kayttaja_id');
                }
        }
        
        public function saveComment() {
            
            $this->form_validation->set_rules('teksti', 'Teksti', 'required');
            $this->form_validation->set_rules('kirjoitus_id', 'Post_id', 'required');
            $this->form_validation->set_rules('kayttaja_id', 'Kayttaja_id', 'required');
            
            if ($this->form_validation->run())
                {
                    $data = $this->input->post();
                    
                    unset($data['formSubmitButton']);
                    unset($data['approved']);

                    $this->load->model('query');

                    $this->query->addComment($data);
                    $post_id = $this->input->post('kirjoitus_id');
                    $this->open($post_id);
                    
                }
                else
                {
                    echo validation_errors();
                }
        }
        
        
        public function open($post_id) {
            $this->load->model('query');   
            $singlePost = $this->query->getSinglePost($post_id);
            $user = $this->query->getUsers();
            $comments = $this->query->getComments($post_id);
            $this->load->view('openPost', ['singlePostVariable'=>$singlePost,'userArray'=>$user, 'commentArray'=>$comments]);
        }
        
        
        
        
        public function getPostToUpdate($post_id) {
            $this->load->model('query');   
            $singlePost = $this->query->getSinglePost($post_id);
            $this->load->view('update', ['singlePostVariable'=>$singlePost]);
        }
        
        
        
        
        public function saveEditedPost($post_id) {
            
            $this->form_validation->set_rules('otsikko', 'otsikko', 'required');
            $this->form_validation->set_rules('teksti', 'teksti', 'required');
            
            if ($this->form_validation->run())
                {
                    $data = $this->input->post();
                    unset($data['formSubmitButton']);
                    
                    // vaihtehtoinen tapa välittää date, jos sitä ei ole databaseen määritelty
                    //$today = date('d-m-Y');
                    //$data['date'] = $today;
                    
                    $this->load->model('query');
                    $this->query->updatePost($data, $post_id);

                    return redirect('Blogi');
                }
                else
                {
                    echo validation_errors();
                }
        }
        
        
        
             
        
        public function delete($post_id) {
            $this->load->model('query');
            $data = $this->input->post();
                    unset($data['formSubmitButton']); // ei välitetä submit-napit valueta
                    unset($data['approved']); // ei välitetä approved-napin valueta
            $this->query->deletePost($data, $post_id);
            return redirect('Blogi');
        }
}

