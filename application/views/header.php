<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">       
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.css'); ?>" ></script>
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css'); ?>" ></script>-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/signin.css'); ?>" ></script>-->
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	<title>CRUD Blogi</title>
</head>

<body>  


    <nav class="navbar navbar-expand-lg navbar-transparent fixed-top">
        <a class="navbar-brand" href="<?php echo base_url('index.php/Blogi') ?>"><h3 class="text-light">CRUD</h3></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="true" aria-label="Toggle navigation" style="">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse collapse show" id="navbarColor03" style="">
        <ul class="navbar-nav mr-auto">
            
<!--          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('index.php/Blogi') ?>">Home <span class="sr-only">(current)</span></a>
          </li>-->
          
          <?php if (!$this->session->userdata('user_id')) { ?>
          <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#loginModal">
                  <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Login / Register <i class="fas fa-user"></i></button>
              </a>
          </li>
          <?php } ?>
          
          <li class="nav-item active">
            <?php if ($this->session->userdata('user_id')) { ?>
                    <?php echo anchor('Account/logout', 'logout  <i class="fas fa-user"></i>', ['class'=>'btn btn-info btn-sm']); ?>
                    <?php } ?>
          </li>
        </ul>
      </div>
    </nav>
    <br>
    <br>
    <br>
    <br>
    <div class="container text-primary">
        <div class="modal" id="myModal" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                            <div class="row">


                                    <?php echo form_open('Account/login', 'class="form-signin"');?>

                                        <?php 
                                            echo form_input(['name'=>'username', 'placeholder'=>'Username', 'class'=>'form-control', 'required'=>'required']);
                                            echo form_error('username','<div class="text-danger">','</div>');

                                            echo form_password(['name'=>'password', 'placeholder'=>'Password', 'class'=>'form-control','required'=>'required']);
                                            echo form_error('password','<div class="text-danger">','</div>');


                                            echo form_submit(['name'=>'submit', 'value'=>'SIGN IN!', 'class'=>'btn btn-sm btn-info btn-block']);
                                        ?>
                                        <br>
                                        <br>
                                        
                                        <p>Don't have an account? <i class="fas fa-users"></i> <?php echo anchor('Account/registerPage', 'Register', ['class'=>'btn btn-sm btn-success btn-block']);?></p>
                                <?php echo form_close();?>
                            </div>

                        <br>
                        <br>

                    </div>


                 </div>
            </div>    
        </div>
</div>
    <br>
