<?php include_once 'header.php'; ?>  
    
    
    <div class="container">
        <?php if ($this->session->userdata('user_id')) { ?>
              <h5 class="text-muted"> Welcome back,  <?php echo $this->session->userdata('username'); ?> !</h5>
                    <?php } ?>
              <br>
              <br>
        <?php if ($this->session->userdata('user_id')) { ?>
                    <?php echo anchor('Blogi/create', 'Write a post !', ['class'=>'btn btn-success']); ?>
                    <?php } ?>

        <table class="table table-hover">
        <thead>
          <tr>
              <th scope="col"><h5 class="text-light">Title</h5></th>
            <th scope="col"><h5 class="text-light">Description</h5></</th>
            <th scope="col"><h5 class="text-light">Date posted</h5></</th>
            <th scope="col"></th>
          </tr>
        </thead>

        <tbody>
            
            <?php if (count($postsArray)): ?>
            <?php foreach(array_reverse($postsArray) as $singlePost): ?>
            
                <tr>
                    
                  <th scope="row"> <?php echo $singlePost->otsikko; ?> </th>
                  <td scope="row"> <?php echo substr($singlePost->teksti,0,30); ?> ...</td>
                  <td scope="row"> <?php echo $singlePost->paivays; ?> </td>

                  <td>
                      <!--muista käyttää tupla-lainausmerkkejä kun polussa on muuttujia-->
                    <?php echo anchor("Blogi/open/{$singlePost->id}", 'Open', ['class'=>'badge badge-success']);?>
                    
                    <?php if ($this->session->userdata('user_id')) { ?>
                    <?php echo anchor("Blogi/getPostToUpdate/$singlePost->id", 'Edit', ['class'=>'badge badge-dark']);?>
                    <?php } ?>
                    
          
                    <?php if ($this->session->userdata('user_id')) { ?>
                        <?php echo anchor("Blogi/delete/$singlePost->id", 'Delete',['class'=>'badge badge-danger']);?>
                    <?php } ?>
                    
                  </td>

                </tr>
            <?php endforeach; ?>
            <?php else: ?>
                
                <tr>
                    <td>Ei kirjoituksia!</td>
                </tr>
            
            <?php endif; ?>
        </tbody>
        </table>
    </div>
<br>
<br>
<?php include_once 'footer.php'; ?>
    
    
    
    
    
    
    

