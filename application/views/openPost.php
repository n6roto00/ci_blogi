<?php include_once 'header.php'; ?>
        
        
        <?php 
            $query = $this->db->get_where('kayttaja', array('id' => $singlePostVariable->kayttaja_id));
            foreach ($query->result() as $user) // TÄHÄN TALLENNETTU KIRJOITTAJAN TIEDOT
         ?>
        <hr>
        <br>
        <br>

    <div class="container"> 
        <?php echo anchor('Blogi', 'Go Back', ['class'=>'btn btn-danger']); ?>
      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Title -->
          <h1 class="mt-4 text-light"><?php  echo $singlePostVariable->otsikko; ?></h1>

          <!-- Author -->
          <p class="lead">
            by
            <?php echo $user->tunnus; ?>
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on <?php  echo $singlePostVariable->paivays; ?></p>

          <hr>

          <!-- Preview Image -->
<!--          <img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">-->

          <hr>

          <!-- Post Content -->
          <p class="lead text-light"><?php  echo $singlePostVariable->teksti; ?></p>


<!--          <blockquote class="blockquote">
            
            <footer class="blockquote-footer">
              <cite title="Source Title">Wikipedia</cite>
            </footer>
          </blockquote>-->

          <hr>
          
    
        
        
        
          <br>
          <br>
    <?php if ($this->session->userdata('user_id')) { ?>

        <div style="width: 400px;">
            <?php echo form_open('Blogi/saveComment') ?>
                <fieldset>

                  <div class="form-group">
                    <?php echo form_textarea(['name'=>'teksti','placeholder'=>'Write your comment...','class'=>'form-group', 'style'=>'width:400px;height:120px']); ?>

                    <div class="col-md-5">
                        <?php echo form_error('comment', '<div class="text-danger">', '</div>'); ?>
                    </div>
                  </div>
                    <input type="hidden" name="kirjoitus_id" value="<?php echo $singlePostVariable->id; ?>" /> 
                    <input type="hidden" name="kayttaja_id" value="<?php echo $this->session->userdata('user_id')?>" />
                    </fieldset>
                  <?php echo form_submit('formSubmitButton', 'Kommentoi!', ['class'=>'btn btn-success']); ?>
                  <?php echo anchor('Blogi', 'Cancel', ['class'=>'btn btn-danger']); ?>
                </fieldset>
            <?php echo form_close(); ?>
        </div>
    <?php } ?> 
    <br>
    <br>   
    
    
    
    
    <?php  
        $query = $this->db->get_where('kommentti', array('kirjoitus_id' => $singlePostVariable->id));
        foreach ($query->result() as $comment): ?>
    
    <?php 
        $query2 = $this->db->get_where('kayttaja', array('id' => $comment->kayttaja_id)); 
        foreach ($query2->result() as $commenter)
    ?>
    <div class="list-group text-light"
                     <!--date-->
                     <div class="card-body">    
                         <div class='card-header'> <?php  echo $comment->paivays; ?> a comment by <?php echo $commenter->tunnus ?><i></i></div>
                        <br>
                        <p class="card-text"><?php echo $comment->teksti;  ?></p>
                     </div>
                </div> <?php
      endforeach; 
    ?>


   
</div>
      </div>
    </div>
<?php include_once 'footer.php'; ?>
