<?php include_once 'header.php'; ?>  

<div class="container">
    <h3 class="text-light">Register</h3>
    <?php echo form_open('Account/registerUser');?>
        <div class="form-group">

                <?php echo form_input(['name'=>'tunnus', 'placeholder'=>'Tunnus', 'class'=>'textbox','required'=>'required']);
                      echo form_error('tunnus','<div class="text-danger">','</div>'); ?>
        </div>
    
    <div class="form-group">
        <?php echo form_input(['name'=>'email', 'placeholder'=>'email', 'class'=>'textbox','required'=>'required']);
                      echo form_error('email','<div class="text-danger">','</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_password(['name'=>'salasana', 'placeholder'=>'Salasana', 'class'=>'textbox','required'=>'required']);
                      echo form_error('salasana','<div class="text-danger">','</div>'); ?>
    </div>
    
    <?php
        echo form_submit(['name'=>'submit', 'value'=>'LUO TUNNUS', 'class'=>'hollow button secondary textbox']); ?>
        <?php echo form_close();?>
</div>
    



<?php include_once 'footer.php'; ?>

