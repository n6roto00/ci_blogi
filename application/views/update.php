<?php include_once 'header.php'; ?>



<div class="container">
    

    <?php echo form_open("Blogi/saveEditedPost/{$singlePostVariable->id}", ['class'=>'form-horizontal']); ?>
    
        <fieldset>
          <!--<legend>Edit post</legend>-->
          
            <br>

            <div class="form-group">
              <label for="exampleInputEmail1">Title</label>

              <?php echo form_input(['name'=>'otsikko','placeholder'=>'Title','class'=>'form-control', 'value'=>set_value('otsikko', $singlePostVariable->otsikko)]); ?>

              <div class="col-md-5">
                  <?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
              </div>
            </div>



            <div class="form-group">
              <label for="description">Message</label>
              <?php echo form_textarea(['name'=>'teksti','placeholder'=>'Teksti','class'=>'form-control', 'value'=>set_value('teksti', $singlePostVariable->teksti)]); ?>

              <div class="col-md-5">
                  <?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
              </div>
            </div>

          
          </fieldset>

          <!--<input type="hidden" name="id" value="<?php echo $this->session->userdata('user_id')?>" />-->  
          <?php echo form_submit('formSubmitButton', 'Save changes', ['class'=>'btn btn-success']); ?>
          <?php echo anchor('Blogi', 'Cancel', ['class'=>'btn btn-danger']); ?>
        </fieldset>
        
    <?php echo form_close(); ?>
</div>




<?php include_once 'footer.php'; ?>



