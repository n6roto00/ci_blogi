<?php include_once 'header.php'; ?>



<div class="container">
        <br>
        <br>
    <?php echo form_open('Blogi/save') ?>
        <fieldset>
          <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            
            <?php echo form_input(['name'=>'otsikko','placeholder'=>'Title of your post','class'=>'form-control', 'maxlength'=>'15']); ?>
            
            <div class="col-md-5">
                <?php echo form_error('otsikko', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
            
            
 
            
            
          
          <div class="form-group">
            <label for="description">Message</label>
            <?php echo form_textarea(['name'=>'teksti','placeholder'=>'Write your message...','class'=>'form-control']); ?>
            
            <div class="col-md-5">
                <?php echo form_error('teksti', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>

          </fieldset>
            <fieldset class="form-group">


            <input type="hidden" name="kayttaja_id" value="<?php echo $this->session->userdata('user_id')?>" />  
            </fieldset>
          <?php echo form_submit('formSubmitButton', 'Post it!', ['class'=>'btn btn-success']); ?>
          <?php echo anchor('Blogi', 'Cancel', ['class'=>'btn btn-danger']); ?>
        </fieldset>
    <?php echo form_close(); ?>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<?php include_once 'footer.php'; ?>



