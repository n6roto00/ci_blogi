-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29.03.2018 klo 09:06
-- Palvelimen versio: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_blog`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `kayttaja`
--

CREATE TABLE `kayttaja` (
  `id` int(11) NOT NULL,
  `tunnus` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `salasana` varchar(255) NOT NULL,
  `etunimi` varchar(50) NOT NULL,
  `sukunimi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `kayttaja`
--

INSERT INTO `kayttaja` (`id`, `tunnus`, `email`, `salasana`, `etunimi`, `sukunimi`) VALUES
(7, 'Diego', 'en.kerro@email.com', '1234', '', '');

-- --------------------------------------------------------

--
-- Rakenne taululle `kirjoitus`
--

CREATE TABLE `kirjoitus` (
  `id` int(11) NOT NULL,
  `otsikko` varchar(50) NOT NULL,
  `teksti` text NOT NULL,
  `kayttaja_id` int(11) NOT NULL,
  `paivays` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `kirjoitus`
--

INSERT INTO `kirjoitus` (`id`, `otsikko`, `teksti`, `kayttaja_id`, `paivays`) VALUES
(23, 'Blogi', 'Tämähän toimii. \r\n\r\nedit: jopa muokkaus toimii', 7, '2018-03-27 12:22:37');

-- --------------------------------------------------------

--
-- Rakenne taululle `kommentti`
--

CREATE TABLE `kommentti` (
  `id` int(11) NOT NULL,
  `teksti` text NOT NULL,
  `paivays` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kirjoitus_id` int(11) NOT NULL,
  `kayttaja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kirjoitus`
--
ALTER TABLE `kirjoitus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kayttaja_id` (`kayttaja_id`);

--
-- Indexes for table `kommentti`
--
ALTER TABLE `kommentti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kayttaja_id` (`kayttaja_id`) USING BTREE,
  ADD KEY `kirjoitus_id` (`kirjoitus_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kirjoitus`
--
ALTER TABLE `kirjoitus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `kommentti`
--
ALTER TABLE `kommentti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `kirjoitus`
--
ALTER TABLE `kirjoitus`
  ADD CONSTRAINT `kirjoitus_ibfk_3` FOREIGN KEY (`kayttaja_id`) REFERENCES `kayttaja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Rajoitteet taululle `kommentti`
--
ALTER TABLE `kommentti`
  ADD CONSTRAINT `kommentti_ibfk_4` FOREIGN KEY (`kayttaja_id`) REFERENCES `kayttaja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kommentti_ibfk_5` FOREIGN KEY (`kirjoitus_id`) REFERENCES `kirjoitus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
